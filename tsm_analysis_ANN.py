
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime

# Importing the datasets with specific headdings
#dataset = pd.read_csv(path + 'prd1tsm.nodes.190125')

# df_prd1tsm_node = pd.read_csv('/Volumes/HDS_Storage/04-TSM/datadump/prd1tsm.nodes.190125', encoding='latin')

# df_prd1tsm_summary = pd.read_csv('/Volumes/HDS_Storage/04-TSM/datadump/prd1tsm.summary.190125',
df_prd1tsm_summary = pd.read_csv('prd1tsm.summary.190125',
                                      names=['START_TIME','END_TIME','ACTIVITY','NUMBER','ENTITY','COMMMETH',
                                             'ADDRESS','SCHEDULE_NAME','EXAMINED','AFFECTED','FAILED','BYTES',
                                             'BYTES_PROTECTED','BYTES_WRITTEN','DEDUP_SAVINGS','COMP_SAVINGS',
                                             'IDLE','MEDIAW','PROCESSES','SUCCESSFUL','VOLUME_NAME','DRIVE_NAME',
                                             'LIBRARY_NAME','LAST_USE','COMM_WAIT','NUM_OFFSITE_VOLS','Duration'])
# removing columns that skew data with meaningless variables.
# yourdf.drop(['columnheading1', 'columnheading2'], axis=1, inplace=True)
df_prd1tsm_summary.drop(['NUMBER', 'ENTITY', 'ADDRESS', 'SCHEDULE_NAME',
                         'MEDIAW', 'VOLUME_NAME', 'DRIVE_NAME', 'LIBRARY_NAME',
                         'LAST_USE', 'NUM_OFFSITE_VOLS', 'Duration'], axis=1, inplace=True)

# converting data values to common date format and creating a duration value of each task
# then converting back to numeric
df_prd1tsm_summary['START_TIME'] = pd.to_datetime(df_prd1tsm_summary['START_TIME'])
df_prd1tsm_summary['END_TIME'] = pd.to_datetime(df_prd1tsm_summary['END_TIME'])
df_prd1tsm_summary['DURATION_TIME'] = df_prd1tsm_summary['END_TIME'] - df_prd1tsm_summary['START_TIME']

df_prd1tsm_summary['DURATION_TIME'] = pd.to_numeric(df_prd1tsm_summary['DURATION_TIME'])


# droping all row entries that are not associated with 'backups' - change for Archives if needed.
df_prd1tsm_summary.drop(df_prd1tsm_summary[df_prd1tsm_summary['ACTIVITY'] != 'BACKUP'].index,
                        inplace=True)

# reordering dataframe for easier use and droped 'Activity'
df_prd1tsm_summary = df_prd1tsm_summary[['COMMMETH', 'EXAMINED', 'AFFECTED', 'FAILED', 'BYTES',
                           'BYTES_PROTECTED', 'BYTES_WRITTEN', 'DEDUP_SAVINGS', 'COMP_SAVINGS',
                           'IDLE', 'PROCESSES', 'COMM_WAIT', 'DURATION_TIME', 'SUCCESSFUL']]

X = df_prd1tsm_summary.values

# Encoding categorical data (names, values etc)
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
le = LabelEncoder()
X[:, 0] = le.fit_transform(X[:, 0])
X[:, 13] = le.fit_transform(X[:, 13])
y = X[:, 13]
X = X[:, :13]

# Splitting the dataset into the training set and the test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc =StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.fit_transform(X_test)


# Part 2 - ANN Creation

# Importing the Keras libaries and packages
import keras



# col = df_prd1tsm_summary_backup.columns.tolist()

df_prd1tsm_summary.head()
