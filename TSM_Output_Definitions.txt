                 NODE_NAME: ULPPRX110
                   NODE_ID: 9
             PLATFORM_NAME: TDP VMware
               DOMAIN_NAME: UNIX
                PWSET_TIME: 2017-11-03 10:39:28.000000
          INVALID_PW_COUNT: 0
                   CONTACT:
               COMPRESSION: CLIENT
                ARCHDELETE: YES
                BACKDELETE: NO
                    LOCKED: NO
              LASTACC_TIME: 2017-11-02 23:43:53.000000
                  REG_TIME: 2017-11-02 23:39:01.000000
                 REG_ADMIN: HDERELAN
         LASTSESS_COMMMETH: Tcp/Ip
            LASTSESS_RECVD: 8796433546
             LASTSESS_SENT: 20923
         LASTSESS_DURATION: 3.84858000000000E+002
         LASTSESS_IDLEWAIT: 4.42200000000000E+000
         LASTSESS_COMMWAIT: 1.91945000000000E+002
        LASTSESS_MEDIAWAIT: 0.00000000000000E+000
            CLIENT_VERSION: 8
            CLIENT_RELEASE: 1
              CLIENT_LEVEL: 2
           CLIENT_SUBLEVEL: 0
       APPLICATION_VERSION: 0
       APPLICATION_RELEASE: 0
         APPLICATION_LEVEL: 0
      APPLICATION_SUBLEVEL: 0
           CLIENT_OS_LEVEL: 3.10.0-514.6.2.el7.
                OPTION_SET:
               AGGREGATION: YES
                       URL:
                  NODETYPE: CLIENT
                   PASSEXP: 0
                   KEEP_MP: NO
            MAX_MP_ALLOWED: 1
            AUTO_FS_RENAME: NO
          VALIDATEPROTOCOL: NO
                  TCP_NAME: ulpprx110.auiag.corp
               TCP_ADDRESS: 10.138.146.142
                      GUID: 17.00.a2.36.86.eb.11.e7.8b.a7.00.50.56.ab.18.c3
               TXNGROUPMAX: 0
             DATAWRITEPATH: ANY
              DATAREADPATH: ANY
        SESSION_INITIATION: ClientOrServer
                CLIENT_HLA:
                CLIENT_LLA:
          COLLOCGROUP_NAME:
              PROXY_TARGET:
               PROXY_AGENT:
             EMAIL_ADDRESS:
             DEDUPLICATION: ClientOrServer
         BACKUP_INITIATION: ALL
                      ROLE: SERVER
                    ROLE_O: USEREPORTED
                   PVENDOR: Intel
                    PBRAND: Xeon
                     PTYPE: 8
                    PMODEL: X6550
                    PCOUNT: 2
                HYPERVISOR: VMware
                      PAPI: NO
                 SCANERROR: NO
                   MACADDR:
                REPL_STATE: ENABLED
                 REPL_MODE: SEND
               REPL_BKRULE: DEFAULT
               REPL_ARRULE: DEFAULT
               REPL_SPRULE: DEFAULT
            CLIENT_OS_NAME: LNX:Red Hat Enterprise Linux Server release 7.3 (Maipo)
CLIENT_SYSTEM_ARCHITECTURE: x64
           CLIENT_PRODUCTS: BA,VE
     CLIENT_TARGET_VERSION:
     CLIENT_TARGET_RELEASE:
       CLIENT_TARGET_LEVEL:
    CLIENT_TARGET_SUBLEVEL:
            AUTHENTICATION: Local
              SSL_REQUIRED: Default
          SESSION_SECURITY: Transitional
          TRANSPORT_METHOD: Unknown
REPLICATION_PRIMARY_SERVER: PRD4TSM
 LAST_REPLICATED_TO_SERVER: PRD54TSM
               ATRISK_TYPE:
           ATRISK_INTERVAL:
       SPLIT_LARGE_OBJECTS: Yes
                 NODEGROUP:
                UTILITYURL:
           RECOVER_DAMAGED: YES
              DECOMM_STATE:
               DECOMM_DATE:










01       START_TIME: 2018-02-02 14:04:41.000000     - start time - Y
02         END_TIME: 2018-02-02 14:05:00.000000     - end time - Y
03         ACTIVITY: BACKUP                         - filter for backup/archive , contains all summary activities - Y
04           NUMBER: 237243                         - process/db number - N
05           ENTITY: SDC1DB498_SQL                  - server, host, db - N
06         COMMMETH: Tcp/Ip                         - communication method - Y
07          ADDRESS: sdc1db498.auiag.corp:62987     - host:port address - N
08    SCHEDULE_NAME: SQL_HRLY_LOGS_0000             - management name of schedule - N
09         EXAMINED: 0                              - # of Object, Files, Examimed as part of backup - Y
10         AFFECTED: 21                             - # of Object, Files, Affected as part of backup - Y
11           FAILED: 0                              - # of Object, Files, that failed as part of backup - Y
12            BYTES: 645530                         - bytes transfered - Y
13  BYTES_PROTECTED: 650347                         - bytes that would be protected - Y
14    BYTES_WRITTEN: 64045                          - bytes that were written as part of backup - Y
15    DEDUP_SAVINGS: 0                              - bytes saved as part of dedup - Y
16     COMP_SAVINGS: 581331                         - total bytes saved as part of dedup - Y
17             IDLE: 15                             - idle waits - Y
18           MEDIAW: 0                              - media wait (tapes) - N
19        PROCESSES: 1                              - processes used - Y
20       SUCCESSFUL: YES                            - status successfull/failed - Y
21      VOLUME_NAME:                                - media volume name (tape) - N
22       DRIVE_NAME:                                - media drive name (tape) - N
23     LIBRARY_NAME:                                - media Library name (tape) - N
24         LAST_USE:                                - media use (tape) - N
25        COMM_WAIT: 0                              - communication waits - Y
26 NUM_OFFSITE_VOLS:                                - media offsite volumes count - N
27         Duration: (customfield)                  - duration calculation - Y




select start_time, TRANSLATE('a bc:de:fg', DIGITS(end_time-start_time), '_______abcdefgh_____',' ') as DURATION, end_time, bytes/1024/1024 as MB, cast(cast((BYTES/1024/1024) as
dec(12,2))/cast((1+(day(end_time-start_time)*86400)+(hour(end_time-start_time)*3600)+(minute(end_time-start_time)*60)+second(end_time-start_time)) as dec(12,2)) as dec(6,2)) as MB_SEC, comm_wait , mediaw
from summary where entity=upper('$1') and start_time   > current_timestamp - $2 hours and bytes/1024/1024    > 500 order by 1





TRANSLATE('a bc:de:fg', DIGITS(end_time-start_time), '_______abcdefgh_____',' ') as DURATION


select *, TRANSLATE('a bc:de:fg', DIGITS(end_time-start_time), '_______abcdefgh_____',' ') as DURATION from summary where start_time \> current_timestamp - 1 day



START_TIME,END_TIME,ACTIVITY,NUMBER,ENTITY,COMMMETH,ADDRESS,SCHEDULE_NAME,EXAMINED,AFFECTED,FAILED,BYTES,BYTES_PROTECTED,BYTES_WRITTEN,DEDUP_SAVINGS,COMP_SAVINGS,IDLE,MEDIAW,PROCESSES,SUCCESSFUL,VOLUME_NAME,DRIVE_NAME,LIBRARY_NAME,LAST_USE,COMM_WAIT,NUM_OFFSITE_VOLS,Duration







